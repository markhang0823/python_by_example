def deco(func):
    def set_red():
        return '\033[31;1m%s\033[0m' % func()
    return set_red

@deco
def hello():
    return 'Hello World!'

if __name__ == '__main__':
    print(hello())
