import random
import string
import sys

def gen_pass(n):
    all_chs = string.ascii_letters + string.digits
    result = [random.choice(all_chs) for i in range(n)]

    return ''.join(result)

if __name__ == '__main__':
    try:
        length = int(sys.argv[1])
    except (IndexError, ValueError):
        print('Usage: %s length' % sys.argv[0])
        exit(1)    # 出现异常，程序结束，退出码设置为非0值

    print(gen_pass(length))
