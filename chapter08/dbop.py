import pymysql

conn = pymysql.connect(
    host='127.0.0.1',
    port=3306,
    user='root',
    passwd='123456',
    db='feizhi',
    charset='utf8'
)
cursor = conn.cursor()
##################################
insert_dep = 'INSERT INTO departments VALUES(%s, %s)'
cursor.executemany(insert_dep, [(1, '人事部')])  # 增加一条记录
# 增加多条记录
deps = [(2, '财务部'), (3, '运维部'), (4, '开发部'), (5, '测试部'), (6, '市场部')]
cursor.executemany(insert_dep, deps)
##################################
select2 = 'SELECT * FROM departments ORDER BY dep_id'
cursor.execute(select2)
cursor.scroll(2, mode='relative')  # 以相对方式向下移动2行记录
print(cursor.fetchone())
print('*' * 20)
cursor.scroll(0, mode='absolute')  # 以绝对方式移动到第1行记录
print(cursor.fetchone())
#############################################
update1 = 'UPDATE departments set dep_name=%s WHERE dep_name=%s'
cursor.execute(update1, ('人力资源部', '人事部'))
#############################################
delete1 = 'DELETE FROM departments WHERE dep_name=%s'
cursor.execute(delete1, ('市场部',))
##################################
conn.commit()
cursor.close()
conn.close()
