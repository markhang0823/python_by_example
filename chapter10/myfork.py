# import os
#
# print('starting...')
# os.fork()  # fork子进程
# print('Hello World!')   # 语句将在父子进程都执行一次

#################################
import os

print('starting...')

retval = os.fork()
if retval:
    print('Hello from parent')
else:
    print('Hello from child')

print('Hello from both!')
